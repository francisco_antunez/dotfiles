#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias vi=vim
alias ls='ls --color=auto'
alias windows='sway'
alias sway_debug='sway -dV &> ~/.swaylog'
alias q='exit'
alias sims='wine ~/.wine/drive_c/Program\ Files\ \(x86\)/Maxis/The\ Sims\ 8\ in\ 1/The\ Sims/Sims.exe -skip_intro -skip_verify -r1920x1080'

PS1='[\u@\h \W]\$ '

[[ -f ~/.Xresources ]] && xrdb -merge -I $HOME ~/.Xresources
#xrdb -merge ~/.Xresources
export EDITOR="vim"
#export XKB_DEFAULT_LAYOUT=us;
#export XKB_DEFAULT_VARIANT=dvp;

# added by Anaconda3 installer
#export PATH="$PATH"
export PATH=/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/lib/jvm/default/bin:~/.npm-global/bin
