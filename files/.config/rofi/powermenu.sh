#!/usr/bin/env bash

## Author : Aditya Shakya (adi1090x)
## Mail : adi1090x@gmail.com
## Github : @adi1090x
## Reddit : @adi1090x
exec aplay ~/Music/System_sounds/button/click_button.wav &
rofi_command="rofi -theme five.rasi"
uptime=$(uptime -p | sed -e 's/up //g')

# Options
shutdown="⏻"
reboot="⟲"
lock="🔒"
suspend="⏾"
logout="<"

# Variable passed to rofi
options="$shutdown\n$reboot\n$lock\n$suspend\n$logout"

chosen="$(echo -e "$options" | $rofi_command -p "UP - $uptime" -dmenu -selected-row 2)"
case $chosen in
    $shutdown)
        systemctl poweroff
        ;;
    $reboot)
        systemctl reboot
        ;;
    $lock)
        swaylock -i ~/Pictures/Wallpaper/lock.png --indicator-radius 200 --indicator-thickness 5 --inside-color 000000A0 --key-hl-color 404040D0 --inside-ver-color 505050F0 --line-color F000002F --ring-color 202020F0 --line-ver-colo    r FFFFFFA0 --line-ver-color FFFFFF --ring-ver-color FFFFFF --text-ver-color FFFFFF
        ;;
    $suspend)
        systemctl suspend
        ;;
    $logout)
		swaymsg exit
        ;;
esac

