#!/bin/bash

declare -A rot=(["right-up"]="90" ["left-up"]="270" ["bottom-up"]="180" ["normal"]="0")

while IFS='$\n' read -r line; do
	orientation="$(echo $line | grep orientation)"
	[[ ! -z $orientation ]] && break
done < <(stdbuf -oL monitor-sensor &)

o="$(echo "$orientation" | grep normal)"
echo $o

if [ -n "$(echo "$orientation" | grep normal)" ]
then
	notify-send normal
elif [ -n "$(echo "$orientation" | grep right-up)" ]
then
	notify-send right-up
elif [ -n "$o" ]
then
	notify-send left-up
elif [ -n "$o" ]
then
	notify-send bottom-up
fi

killall monitor-sensor
