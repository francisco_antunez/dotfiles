#!/bin/bash

function rotate_ms {
    swaymsg output eDP-1 transform "${rot[$1]}"
}

# Thank you
# fishonadish
# https://bbs.archlinux.org/viewtopic.php?id=243005
# https://gitlab.com/snippets/1793649
# lines 65-68
function start {
    while IFS='$\n' read -r line; do
        rotation="$(echo $line | sed -En "s/^.*orientation changed: (.*)/\1/p")"
        [[ !  -z  $rotation  ]] && rotate_ms $rotation
    done < <(stdbuf -oL monitor-sensor &)
}

function stop {
    if [ -f PID ];
    then
        pid=$(cat PID)
        rm PID
        killall monitor-sensor
        pkill -g $pid
    else
        echo "no pid found"
    fi
}

TBLT=$1
declare -A rot=(["right-up"]="90" ["left-up"]="270" ["bottom-up"]="180" ["normal"]="0")

if [ "$TBLT" = "on" ];
then 
    notify-send $$
    echo $$ > PID
    start

elif [ "$TBLT" = "off" ];
then 
    stop
fi