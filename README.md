# Configuration Files
Configurations included are:
- .bash_profile
- .bashrc
- mako
- rofi
- sway
- waybar

___

## Apply/Restore Configuration
Clone the repo

    git clone https://gitlab.com/francisco_antunez/dotfiles.git

Run the provided script.

    ./explode.sh
This creates symbolic links in the structure stored in the *files* folder inside the user home (`~/`) folder.<br>
 No file is overwritten with this script, and folders that don't exist are created.

---

 ### Credit
 Some config files included are written by other people, and their information has been left in those files.
